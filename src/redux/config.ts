import { typeStorageKeys, typeSessionStorage } from './types';

export const token: string = 'popeyes_token';
export const storageKeys: typeStorageKeys = {
  USER_TOKEN: 'popeyes_token',
  USER_DATA: 'popeyes_user',
};

export const sessionStorageKey: typeSessionStorage = {
  NOTIFICATION_KEY: 'notification_key',
  POPUP_SUCCESS_COMBO: 'popup_success_combo',
};
