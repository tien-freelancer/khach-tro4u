import { combineReducers } from 'redux';

import { interfaceRootReducer } from './types';

// import reducers
import loading from './reducers/loading';
import auth from './reducers/auth';
import contract from './reducers/contract';

const rootReducer = combineReducers<any>({
    loading,
    auth,
    contract,
});

export type AppState = ReturnType<typeof rootReducer>;
export default rootReducer;
