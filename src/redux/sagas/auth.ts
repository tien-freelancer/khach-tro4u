import { all, call, delay, put, takeLatest } from "redux-saga/effects";
import { login } from "../../api/auth";
import { authActionTypes } from "../actions";
import { createFailureActionType, createSuccessActionType } from "../helper";
const {  AUTH_LOGIN } = authActionTypes;


function* _loginSaga(action: any): any {
  const { data, onComplete, onFailure } = action.payload;
  try {
    const response: any = yield call(login, data);
    yield put({
      type: createSuccessActionType(AUTH_LOGIN),
      response: response.data,
    });
    onComplete(response);
  } catch (error) {
    yield put({
      type: createFailureActionType(AUTH_LOGIN),
      message: error,
      response: false,
      success: false,
    });
    onFailure(error);
    yield delay(3000);
    yield put({
      type: createFailureActionType(AUTH_LOGIN),
      success: null,
    });
  }
}

function* authSaga() {
  yield all([
    takeLatest(AUTH_LOGIN, _loginSaga),

  ])
}

export default authSaga;
