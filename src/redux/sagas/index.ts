import { all, fork } from 'redux-saga/effects';
import authSaga from './auth';
import contractSaga from './contract';

export function* rootSaga() {
    yield all([
        // all saga in here
        fork(authSaga),
        fork(contractSaga),
    ]);
}
