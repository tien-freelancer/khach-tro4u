import { all, call, put, takeLatest } from 'redux-saga/effects';
import { contractDetail } from '../../api/contract';
import { contractActionTypes } from '../actions';
import { createFailureActionType, createSuccessActionType } from '../helper';
const { CONTRACT_DETAIL } = contractActionTypes;

function* _getContractDetail(action: { payload: { id: number } }): any {
    try {
        const response: any = yield call(contractDetail, action.payload);
        yield put({
            type: createSuccessActionType(CONTRACT_DETAIL),
            response: response.data,
        });
    } catch (error) {
        yield put({
            type: createFailureActionType(CONTRACT_DETAIL),
            response: {
                data: [],
                message: error,
            },
        });
    }
}

function* contractSaga() {
    yield all([takeLatest(CONTRACT_DETAIL as any, _getContractDetail)]);
}

export default contractSaga;
