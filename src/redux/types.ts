export type typeStorageKeys = {
  USER_TOKEN: string;
  USER_DATA: string;
};


export interface initial {
  requesting: boolean;
  success: boolean;
  message: any;
  response: any;
}

export interface interfaceAuth {
  login: initial;
}

export interface interfaceRootReducer {
  auth: interfaceAuth;
  loading: any;
}

export type typeSessionStorage = {
  NOTIFICATION_KEY: string;
  POPUP_SUCCESS_COMBO: string;
};
