import { createFailureActionType, createSuccessActionType } from "../helper";
import { initial, interfaceAuth } from "../types";
import { handleActions } from "redux-actions";
import { authActionTypes } from "../actions";

const initialState: interfaceAuth = {
  login: <initial><unknown>{
    requesting: false,
    success: null,
    message: null,
    response: null,
  }
};

const { AUTH_LOGIN } = authActionTypes;

const auth = handleActions(
  {
    [createSuccessActionType(AUTH_LOGIN)]: (state: any, { response }: any) => {
      return {
        ...state,
        login: {
          ...state.login,
          message: "success",
          requesting: false,
          success: true,
          response: response,
        },
      };
    },
    [createFailureActionType(AUTH_LOGIN)]: (
      state: any,
      { message, response, success } : any
    ) => {
      return {
        ...state,
        login: {
          ...state.login,
          message: message,
          requesting: false,
          success: success,
          response: response,
        },
      };
    },
  },
  initialState
);
export default auth;
