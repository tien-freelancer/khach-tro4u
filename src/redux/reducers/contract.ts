import { createFailureActionType, createSuccessActionType } from '../helper';
import { handleActions } from 'redux-actions';
import { contractActionTypes } from '../actions';
type contractDetailType = {
    data: Array<any>;
    messages: string;
    status?: boolean;
};

interface initialStateInterface {
    contractDetail: contractDetailType;
}

const initialState: initialStateInterface = {
    contractDetail: {
        data: [],
        status: false,
        messages: '',
    },
};

const { CONTRACT_DETAIL } = contractActionTypes;

const auth = handleActions(
    {
        [createSuccessActionType(CONTRACT_DETAIL)]: (state: any, { response }: any) => {
            return {
                ...state,
                contractDetail: {
                    data: response.data,
                    status: response.status,
                },
            };
        },
        [createFailureActionType(CONTRACT_DETAIL)]: (state: any, { message, response, success }: any) => {
            return {
                ...state,
                message: 'error',
            };
        },
    },
    initialState,
);
export default auth;
