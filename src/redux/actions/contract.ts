import { Action } from 'redux';
import { createAction } from 'redux-actions';

export const actionTypes = {
    CONTRACT_DETAIL: 'contract/CONTRACT_DETAIL',
};

export const actions = {
    contractDetail: createAction(actionTypes.CONTRACT_DETAIL),
};
