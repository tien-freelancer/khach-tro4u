import { Action } from "redux";
import { createAction } from "redux-actions";

export const actionTypes = {
    AUTH_LOGIN: "auth/AUTH_LOGIN",
};

export const actions = {
    loginAction: createAction(actionTypes.AUTH_LOGIN),
}