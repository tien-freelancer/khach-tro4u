
export const maybe = (exp: Function, d: any) => {
  try {
    const result = exp();
    return result === undefined ? d : result;
  } catch {
    return d;
  }
};

export const getGalleryBySlug = (galleries: Array<any> | any, slug: string) => {
  if (galleries) {
    const gallery = galleries.find((item: any) => item.slug === slug);
    return gallery;
  }
  return null;
};

export const getMenuItemsBySlug = (menuData: any, slug: any) => {
  if (menuData) {
    const mainMenu = menuData.find((menu: any) => menu.slug === slug);
    if (mainMenu?.menu_nodes_parent)
      return mainMenu?.menu_nodes_parent.map((mainMenuItem: any) => {
        return {
          ...mainMenuItem,
          // url: isDesktop ? mainMenuItem : "abc"
          // name: mainMenuItem.title,
          // children: mainMenuItem?.children?.map(childrenItem => ({ ...childrenItem, name: childrenItem.title}))
        };
      });
  }
  return [];
};

export const formatNumber = (value: any) => {
  if (value) {
    const decimalPosition = value.toString().indexOf(".");
    if (decimalPosition > 0) {
      const intVal = value.toString().substring(0, decimalPosition);
      const decimalVal = value.toString().substring(decimalPosition + 1);
      return `${intVal.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}.${decimalVal}`;
    }
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }
  return "";
};


export const cleanObject = (obj: any) => {
  let result: any = {};
  if (obj) {
    Object.keys(obj).forEach((key) => {
      if ((!Array.isArray(obj[key]) && obj[key]) || obj[key]?.length) result[key] = obj[key];
    });
  }
  return result;
};