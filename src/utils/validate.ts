export const patternNull = /\s+$/;
export const patternEmail = /^([A-Z\w\d\._-]{3,32})@([a-z0-9\-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/;
export const patternPhone = /(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})\b/;
export const patternPassword = /([^\s]{8,})$/;
export const patternNull1 = /^\s*$/;
