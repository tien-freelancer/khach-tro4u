import { Fragment, useCallback, useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import './App.css';
// import Layout from './components/Layout';
import { useNavigate } from 'react-router-dom';
import DipositLayout from './components/Layout/DipositLayout';
import InvoiceLayout from './components/Layout/InvoiceLayout';
import { storageKeys } from './constants';
import Contract from './page/Contract';
import Diposit from './page/Diposit';
import Home from './page/Home';
import Invoice from './page/Invoice';
import { Login } from './page/Login/Login';
import Problem from './page/Problem';
import { getStringData } from './utils/sessionStorage';
import PageNotFound from './page/PageNotFound';

const publicRoutes = [
    { path: '/login', component: Login, layout: null },
    { path: '/', component: Home, layout: InvoiceLayout },
    { path: '/hoadon', component: Invoice, layout: InvoiceLayout },
    { path: '/hopdong', component: Contract, layout: InvoiceLayout },
    { path: '/suco', component: Problem, layout: InvoiceLayout },
    { path: '/khach-coc', component: Diposit, layout: DipositLayout },
];

function App() {
    const navigate = useNavigate();

    const checkLogin = useCallback(async () => {
        const phoneNumber = await getStringData(storageKeys.USER_PHONE_NUMBER);
        if (!phoneNumber) {
            navigate('/login');
            return;
        }
    }, []);

    useEffect(() => {
        checkLogin();
    }, []);

    return (
        <div className="App">
            <Routes>
                {publicRoutes.map((route, index) => {
                    const Page = route.component;
                    let Layout: any;

                    if (route.layout) {
                        Layout = route.layout;
                    } else if (route.layout === null) {
                        Layout = Fragment;
                    }

                    return (
                        <Route
                            key={index}
                            path={route.path}
                            element={
                                <Layout>
                                    <Page />
                                </Layout>
                            }
                        />
                    );
                })}

                <Route path="*" element={<PageNotFound />} />
            </Routes>
        </div>
    );
}

export default App;
