import HelpIcon from '@mui/icons-material/Help';
import styles from './PageNotFound.module.scss';

function PageNotFound() {
    return (
        <div className="container">
            <div className={styles.boxNotFound}>
                <HelpIcon />
                <div className={styles.title}>Page Not Found</div>
            </div>
        </div>
    );
}

export default PageNotFound;
