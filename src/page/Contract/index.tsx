import { useEffect } from 'react';
import { Dispatch } from 'redux';
import { useDispatch, useSelector } from 'react-redux';
import { contractActionTypes } from '../../redux/actions';
import { AppState } from '@/redux/rootReducer';

const Contract = () => {
    const dispatch: Dispatch = useDispatch();
    const contract = useSelector((state: AppState) => state.contract);
    console.log(contract);

    useEffect(() => {
        dispatch({
            type: contractActionTypes.CONTRACT_DETAIL,
            payload: {
                id_hop_dong: 3059,
            },
        });
    }, []);

    return (
        <>
            <div className="container">
                <h1>Trang Hợp đồng nè</h1>
            </div>
        </>
    );
};

export default Contract;
