import { Box, Button, Stack, Step, StepContent, StepLabel, Stepper, Typography } from '@mui/material';
import styles from './Invoice.module.scss';
import { useState } from 'react';

import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
import DescriptionIcon from '@mui/icons-material/Description';
import PriceCheckIcon from '@mui/icons-material/PriceCheck';
import EditCalendarIcon from '@mui/icons-material/EditCalendar';
import ContactMailIcon from '@mui/icons-material/ContactMail';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import AlarmOnIcon from '@mui/icons-material/AlarmOn';
import CurrencyExchangeIcon from '@mui/icons-material/CurrencyExchange';
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';

import EastIcon from '@mui/icons-material/East';

import { Divider } from '@mui/material';

function Invoice() {
    return (
        <>
            <div className="container">
                <div className={styles.wrapper}>
                    <div className={`${styles.boxLeft} ${styles.boxInvoice}`}>
                        <div className={styles.topContent}>
                            <p className={styles.title}>
                                Hóa đơn <span className={styles.day}>01/2024</span>
                            </p>
                            <p>20/12024 - 20/2/204</p>

                            <div className={`${styles.icon} ${styles.iconPrev}`}>
                                <KeyboardBackspaceIcon />
                            </div>
                            <div className={`${styles.icon} ${styles.iconNext}`}>
                                <EastIcon />
                            </div>
                        </div>
                        <div className={styles.boxContent}>
                            <div className={styles.listTask}>
                                <div className={styles.itemTask}>
                                    <div className={styles.info}>
                                        <div className={styles.name}>Thuê</div>
                                        <div className={styles.price}>70.000.000 </div>
                                    </div>
                                    <div className={styles.desc}>
                                        <div className={styles.left}>
                                            31 ngày <span>Đơn giá 3.500.000</span>
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.itemTask}>
                                    <div className={styles.info}>
                                        <div className={styles.name}>Điện theo đồng hồ 1</div>
                                        <div className={styles.price}>100.000 </div>
                                    </div>
                                    <div className={styles.desc}>
                                        <div className={styles.left}>
                                            46234 - 45678 <span> = 2000 x Đơn giá 25.000</span>
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.itemTask}>
                                    <div className={styles.info}>
                                        <div className={styles.name}>Tiền nước</div>
                                        <div className={styles.price}>100.000 </div>
                                    </div>
                                    <div className={styles.desc}>
                                        <div className={styles.left}>
                                            46234 - 45678 <span> = 2000 x Đơn giá 25.000</span>
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.itemTask}>
                                    <div className={styles.info}>
                                        <div className={styles.name}>Tiền xe</div>
                                        <div className={styles.price}>100.000 </div>
                                    </div>
                                    <div className={styles.desc}>
                                        <div className={styles.left}>
                                            46234 - 45678 <span> = 2000 x Đơn giá 25.000</span>
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.itemTask}>
                                    <div className={styles.info}>
                                        <div className={styles.name}>Tiền Net</div>
                                        <div className={styles.price}>100.000 </div>
                                    </div>
                                    <div className={styles.desc}>
                                        <div className={styles.left}>
                                            46234 - 45678 <span> = 2000 x Đơn giá 25.000</span>
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.itemTask}>
                                    <div className={styles.info}>
                                        <div className={styles.name}>Tiền Rác</div>
                                        <div className={styles.price}>100.000 </div>
                                    </div>
                                    <div className={styles.desc}>
                                        <div className={styles.left}>
                                            46234 - 45678 <span> = 2000 x Đơn giá 25.000</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <Divider />

                            <div className={styles.boxTotal}>
                                <div className={styles.item}>
                                    <div className={styles.name}>Tổng tiền HĐ</div>
                                    <div className={styles.price}>3.000.000</div>
                                </div>
                                <div className={styles.item}>
                                    <div className={styles.name}>Nợ tháng trước </div>
                                    <div className={styles.price}>100.000</div>
                                </div>
                                <div className={styles.item}>
                                    <div className={styles.name}>Phạt chậm thanh toán </div>
                                    <div className={styles.price}>100.000</div>
                                </div>
                                <div className={styles.item}>
                                    <div className={styles.name}>Khuyến mãi </div>
                                    <div className={styles.price}>200.000</div>
                                </div>
                            </div>
                            <Divider />

                            <div className={styles.totalPrice}>
                                <div className={styles.name}>Tổng tiền</div>
                                <div className={styles.price}>
                                    <span className="color-main"> 86.003.032</span>
                                </div>
                            </div>

                            {/* <Divider /> */}

                            <Stack
                                className={styles.listBtn}
                                spacing={{ xs: 1, sm: 2 }}
                                direction="row"
                                flexWrap="wrap"
                                justifyContent="center"
                            >
                                <Button variant="contained" color="error" startIcon={<EditCalendarIcon />}>
                                    Hẹn thanh toán
                                </Button>
                                <Button variant="contained" startIcon={<ContactMailIcon />}>
                                    Báo chuyển khoản
                                </Button>
                            </Stack>
                        </div>

                        <div className={`${styles.boxHistory} box-border`}>
                            <div className="title-main">Lịch sử thanh toán</div>

                            <div className={styles.listTask}>
                                <div className={styles.itemTask}>
                                    <div className={styles.info}>
                                        <div className={styles.name}>Đã thanh toán Tiền mặt</div>
                                        <div className={styles.price}>50.000.000 </div>
                                    </div>
                                    <div className={styles.desc}>
                                        <div className={styles.day}>20/11/2023</div>
                                        <div className={`${styles.status} ${styles.success}`}>Hoàn thành</div>
                                    </div>
                                </div>
                                <div className={styles.itemTask}>
                                    <div className={styles.info}>
                                        <div className={styles.name}>Đã thanh toán Chuyển khoản</div>
                                        <div className={styles.price}>50.000.000 </div>
                                    </div>
                                    <div className={styles.desc}>
                                        <div className={styles.day}>20/11/2023</div>
                                        <div className={`${styles.status} ${styles.warning}`}>Chờ xác nhận</div>
                                    </div>
                                </div>
                                <div className={styles.itemTask}>
                                    <div className={styles.info}>
                                        <div className={styles.name}>Đã thanh toán Tiền mặt</div>
                                        <div className={styles.price}>70.000.000 </div>
                                    </div>
                                    <div className={styles.desc}>
                                        <div className={styles.day}>20/11/2023</div>
                                        <div className={`${styles.status} ${styles.error}`}>Từ chối</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className={`${styles.boxRight} ${styles.boxPayment} box-border`}>
                        <div className={styles.boxContent}>
                            <div className="title-main">THANH TOÁN TIỀN CHUYỂN KHOẢN</div>

                            <div className={styles.boxStep}>
                                <div className={styles.itemStep}>
                                    <div className={styles.icon}>
                                        <AccountBalanceIcon />
                                    </div>
                                    <div className={styles.content}>
                                        <div className={styles.title}>Bước 1: Chuyển khoản về ngân hàng ACB</div>
                                        <div className={styles.desc}>
                                            STK: 91881 - Phan Thi Kim Loan Nội dung: ghi rõ Số điện thoại hoặc Mã phòng
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.itemStep}>
                                    <div className={styles.icon}>
                                        <NotificationsActiveIcon />
                                    </div>
                                    <div className={styles.content}>
                                        <div className={styles.title}>Bước 2: Báo chuyển khoản</div>
                                        <div className={styles.desc}>
                                            Đăng nhập vào website: khach.nhatro.3t.com Nhấn vào nút Báo chuyển khoản
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.itemStep}>
                                    <div className={styles.icon}>
                                        <DescriptionIcon />
                                    </div>
                                    <div className={styles.content}>
                                        <div className={styles.title}>Bước 3: Cung cấp sao kê</div>
                                        <div className={styles.desc}>Nhập số tiền đã chuyển khoản kèm hình sao kê</div>
                                    </div>
                                </div>
                                <div className={styles.itemStep}>
                                    <div className={styles.icon}>
                                        <PriceCheckIcon />
                                    </div>
                                    <div className={styles.content}>
                                        <div className={styles.title}>Bước 4: Xác nhận tiền</div>
                                        <div className={styles.desc}>Kế toán sẽ kiểm tra sao kê và xác nhận</div>
                                    </div>
                                </div>
                            </div>

                            <div className="title-main">THANH TOÁN BẰNG TIỀN MẶT</div>
                            <div className={styles.boxStep}>
                                <div className={styles.itemStep}>
                                    <div className={styles.icon}>
                                        <CheckCircleOutlineIcon />
                                    </div>
                                    <div className={styles.content}>
                                        <div className={styles.title}>Bước 1: Hẹn thanh toán</div>
                                        <div className={styles.desc}>Nhấn vào nút hen thanh toán bên trên</div>
                                    </div>
                                </div>
                                <div className={styles.itemStep}>
                                    <div className={styles.icon}>
                                        <AlarmOnIcon />
                                    </div>
                                    <div className={styles.content}>
                                        <div className={styles.title}>Bước 2: Chọn thời gian phù hợp</div>
                                        <div className={styles.desc}>
                                            Nhập thời gian phù hợp để nhân viên qua thu trực tiếp
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.itemStep}>
                                    <div className={styles.icon}>
                                        <CurrencyExchangeIcon />
                                    </div>
                                    <div className={styles.content}>
                                        <div className={styles.title}>Bước 3: Nhân viên đến thu tiền</div>
                                        <div className={styles.desc}>
                                            Đến thời gian hẹn nhân viên sẽ gọi điện liên hệ để thu tiền Lưu ý phụ thu
                                            10.000đ/lần
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Invoice;
