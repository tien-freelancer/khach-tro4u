import * as React from 'react';
import { Error } from '@mui/icons-material';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import { Button, Dialog, DialogContent, InputAdornment, TextField } from '@mui/material';
import { useCallback, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import { Dispatch } from 'redux';
import images from '../../assets/images';
import { storageKeys } from '../../constants';
import { authActionTypes } from '../../redux/actions';
import { setStringData } from '../../utils/sessionStorage';
import styles from './Login.module.scss';
import LoginIcon from '@mui/icons-material/Login';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';

interface LoginForm {
    phone: string;
}

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export function Login() {
    const navigate = useNavigate();
    const [arrContract, setArrContract] = useState([]);
    const [isActiveBtn, setIsActiveBtn] = useState(false);

    const dispatch: Dispatch = useDispatch();
    const {
        register,
        handleSubmit,
        formState: { errors },
        watch,
        setError,
    } = useForm({
        defaultValues: {
            phone: '',
        },
    });

    const onSubmit = useCallback(
        (values: LoginForm) => {
            // const { phone } = watch();
            dispatch({
                type: authActionTypes.AUTH_LOGIN,
                payload: {
                    data: {
                        phone: values.phone,
                    },
                    onComplete: (res: any) => {
                        const data: any = res.data;
                        console.log('data', data.data);

                        if (data.status == 'true' && data.data.allHopDong.length == 1) {
                            //TODO save phone number to Session storage
                            setStringData(storageKeys.USER_PHONE_NUMBER, values.phone);

                            //TODO ridrect to next page
                            navigate('/');
                            const contractActive = data.data.allHopDong.filter(
                                (e: any) => e.tinh_trang_hop_dong != 'Báo trả',
                            );

                            setArrContract(contractActive);
                            console.log('lenght', data.data.allHopDong.length);
                        } else if (data.data.allHopDong.length > 1) {
                            setStringData(storageKeys.USER_PHONE_NUMBER, values.phone);

                            const contractActive = data.data.allHopDong.filter(
                                (e: any) => e.tinh_trang_hop_dong != 'Báo trả',
                            );
                            setArrContract(contractActive);
                        } else if ((data.data.allHopDong.tinh_trang_hop_dong = 'Báo trả')) {
                            console.log('bao tra');
                        }

                        setError('phone', { type: 'validate', message: data.message });
                    },
                    onFailure: (err: any) => {
                        //TODO show error
                        setError('phone', { type: 'validate', message: 'Số điện thoại không đúng' });
                    },
                },
            });
        },
        [watch],
    );

    return (
        <>
            {arrContract.length > 1 && (
                <Dialog
                    maxWidth="xs"
                    open={true}
                    onClose={() => {}}
                    TransitionComponent={Transition}
                    keepMounted
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    className={styles.modalSearch}
                >
                    <DialogContent>
                        <div className={styles.logo3t}>
                            <img src={images.logo3T} alt="KhachTro4U" />
                        </div>
                        <p className="text-center color-gray">Vui lòng chọn phòng muốn xem </p>
                        <br />
                        <div className={styles.listContract}>
                            {arrContract.map((e: any, index) =>
                                // let linkToe = '';
                                e.tinh_trang_hop_dong == 'Cho thuê' ? (
                                    <Link to={`/hopdong?${e.id}`} key={index} className={styles.item}>
                                        <p className={styles.nameRoom}>
                                            {e.phong.loai} {e.phong.ten}
                                        </p>
                                        <p>Địa chỉ: {e.phong.nha.dia_chi}</p>
                                    </Link>
                                ) : (
                                    <Link to={`/khach-coc`} key={index} className={styles.item}>
                                        <p className={styles.nameRoom}>
                                            {e.phong.loai} {e.phong.ten}
                                        </p>
                                        <p>Địa chỉ: {e.phong.nha.dia_chi}</p>
                                    </Link>
                                ),
                            )}
                        </div>

                        <p className={styles.hotline}>HOTLINE: 0398.771.881</p>
                    </DialogContent>
                </Dialog>
            )}

            <form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                <div className={styles.login_container}>
                    <div className={styles.login_form}>
                        <div className={styles.logoLogin}>
                            <img src={images.logoLogin} alt="KhachTro4U" />
                        </div>

                        <TextField
                            {...register('phone', {
                                required: true,
                                pattern: /^[0-9]{10,11}$/g,
                                // maxLength: 10,
                            })}
                            inputProps={{
                                maxLength: 10,
                            }}
                            onKeyDown={(event) => {
                                if (event.key === 'Enter') {
                                    // Do code here
                                    // event.preventDefault();
                                }
                            }}
                            className={styles.inputLogin}
                            label="Số điện thoại"
                            name="phone"
                            onChange={(e) => {
                                const regex = /^[0-9]{10}$/g;
                                setIsActiveBtn(regex.test(e.target.value));
                            }}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <PhoneAndroidIcon />
                                    </InputAdornment>
                                ),
                            }}
                        />
                        {errors.phone && (
                            <span className={styles.errorMessage}>
                                <Error />
                                {errors.phone.message || 'Vui lòng nhập đúng định dạng số điện thoại'}
                            </span>
                        )}
                        <Button
                            className={styles.buttonLogin}
                            disabled={!isActiveBtn}
                            size="large"
                            variant="contained"
                            type="submit"
                        >
                            <LoginIcon /> Đăng nhập
                        </Button>

                        <div className={styles.note}>
                            <p>Lưu ý</p>
                            <p>Chỉ áp dụng số điện thoại đã đăng ký</p>
                            <p className={styles.hotline}>
                                HOTLINE: <a href="tel:0398771881">0398 771 881</a>
                            </p>
                        </div>
                    </div>
                </div>
            </form>
        </>
    );
}
