import { FormControl } from '@mui/material';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import { Button, InputAdornment, TextField } from '@mui/material';
import { useState } from 'react';
import images from '../../assets/images';
import styles from './Login.module.scss';
import { memo } from 'react';

function Login({ checkLogin }: any) {
    const [isLogin, setIsLogin] = useState(false);

    return (
        <div className={styles.login_container}>
            <div className={styles.login_form}>
                <div className={styles.logoLogin}>
                    <img src={images.logoLogin} alt="KhachTro4U" />
                </div>

                <TextField
                    className={styles.inputLogin}
                    label="Số điện thoại"
                    type="number"
                    required
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <PhoneAndroidIcon />
                            </InputAdornment>
                        ),
                    }}
                />

                <Button onClick={checkLogin} className={styles.buttonLogin} size="large" variant="contained">
                    Đăng nhập
                </Button>

                <div className={styles.note}>
                    <p>Lưu ý</p>
                    <p>Chỉ áp dụng số điện thoại đã đăng ký</p>
                    <p className={styles.hotline}>
                        HOTLINE: <a href="tel:0398771881">0398 771 881</a>
                    </p>
                </div>
            </div>
        </div>
    );
}

export default memo(Login);
