import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import LibraryBooksIcon from '@mui/icons-material/LibraryBooks';
import LogoutIcon from '@mui/icons-material/Logout';
import NotificationsIcon from '@mui/icons-material/Notifications';
import PersonIcon from '@mui/icons-material/Person';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import ReceiptIcon from '@mui/icons-material/Receipt';
import SearchIcon from '@mui/icons-material/Search';
import WarningIcon from '@mui/icons-material/Warning';
import HomeIcon from '@mui/icons-material/Home';
import Divider from '@mui/material/Divider';

import { Box, Button, Dialog, DialogContent, DialogTitle, Stack, TextField } from '@mui/material';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import styles from './Header.module.scss';
import images from '../../../../assets/images';
import { removeItem } from '../../../../utils/sessionStorage';
import { storageKeys } from '../../../../constants';
import ArticleIcon from '@mui/icons-material/Article';
import AnchorIcon from '@mui/icons-material/Anchor';

function HeaderDiposit() {
    // const [searchResult, setSearchResult] = useState([])
    const [open, setOpen] = useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const logOut = async () => {
        await removeItem(storageKeys.USER_PHONE_NUMBER);
        window.location.reload();
    };

    return (
        <header className={styles.header}>
            <div className="container">
                <div className={styles.boxHeader}>
                    <Link to="/" className={styles.logo}>
                        <img src={images.logo} alt="" />
                    </Link>

                    <div className={styles.menuHeader}>
                        <div className={`${styles.itemMenu} ${styles.active} ${styles.hasSub}`}>
                            <Link to="/">
                                <HomeIcon />
                                <span>Trang chủ </span>
                            </Link>
                        </div>

                        <div className={styles.itemMenu}>
                            <Link to="/khach-coc">
                                <AnchorIcon />
                                <span>Phiếu cọc</span>
                            </Link>
                        </div>
                        <div className={styles.itemMenu}>
                            <Link to="/mauhopdong">
                                <ArticleIcon />
                                <span>Mẫu hợp đồng thuê</span>
                            </Link>
                        </div>
                    </div>

                    <div className={styles.actionRight}>
                        <div className={`${styles.hasSub} ${styles.item}`} onClick={handleClickOpen}>
                            <SearchIcon />

                            <Dialog
                                open={open}
                                onClose={handleClose}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                                sx={{
                                    '& .MuiDialog-container': {
                                        alignItems: 'flex-start',
                                        '& .MuiPaper-root': {
                                            width: '100%',
                                            maxWidth: '500px',
                                        },
                                    },
                                }}
                                className={styles.modalSearch}
                            >
                                <DialogTitle>Tìm kiếm</DialogTitle>
                                <DialogContent>
                                    <Stack spacing={2} direction="row">
                                        <TextField
                                            autoFocus
                                            required
                                            name="text"
                                            label="Nhập từ khóa cần tìm"
                                            type="text"
                                            fullWidth
                                            variant="standard"
                                        />

                                        <Button variant="contained" size="small" className={styles.btnSearch}>
                                            Tìm kiếm
                                        </Button>
                                    </Stack>
                                </DialogContent>
                            </Dialog>
                        </div>
                        <div className={`${styles.hasSub} ${styles.item}`}>
                            <NotificationsIcon />

                            <Box className={styles.subMenu}>
                                <ul>
                                    <li>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Accusamus impedit</li>
                                    <li>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Accusamus impedit</li>
                                    <li>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Accusamus impedit</li>
                                </ul>
                            </Box>
                        </div>

                        <div className={`${styles.hasSub} ${styles.item}`}>
                            <AccountCircleIcon />
                            <Box className={styles.subMenu}>
                                <div className={styles.avatar}>
                                    <img src="https://dummyimage.com/50x50/bcbcbc" alt="" />
                                </div>
                                <ul>
                                    <li>
                                        <PersonIcon /> Vu Tien
                                    </li>
                                    <li>
                                        <PhoneAndroidIcon /> 0398822724
                                    </li>
                                    <Divider />
                                    <li>
                                        <span className={styles.btnLogout} onClick={() => logOut()}>
                                            <LogoutIcon />
                                            Đăng xuất
                                        </span>
                                    </li>
                                </ul>
                            </Box>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
}

export default HeaderDiposit;
