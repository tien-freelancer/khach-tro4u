import { Divider } from '@mui/material';
import styles from './Footer.module.scss';
import { Link } from 'react-router-dom';

const Footer = () => {
    return (
        <>
            <footer className="footer">
                <div className="container">
                    <div className={styles.boxFooter}>
                        <div className={`${styles.itemFooter} ${styles.itemAbout}`}>
                            <div className={styles.titleFooter}>GIỚI THIỆU</div>
                            <p>
                                Nhà Trọ 3T chuyên quản lý vận hành căn hộ dịch vụ, phòng trọ, dãy trọ, ký túc xá,
                                sleepbox tại thành phố Hồ Chí Minh.
                            </p>
                        </div>
                        <div className={`${styles.itemFooter} ${styles.itemLink}`}>
                            <div className={styles.titleFooter}>THÔNG TIN</div>
                            <p>
                                <Link to="/hoptac">Hóa đơn </Link>
                            </p>

                            <p>
                                <Link to="/dieukhoan">Điều khoản </Link>
                            </p>
                        </div>
                        <div className={`${styles.itemFooter} ${styles.itemContact}`}>
                            <div className={styles.titleFooter}>LIÊN HỆ</div>
                            <p>Phone: 0398 771 881</p>
                            <p>Zalo: 0398 771 88</p>
                            <p>Web: khach.nhatro3t.com</p>
                        </div>
                    </div>
                    <Divider />
                    <div className={styles.copyright}>
                        Copyright© 2024{' '}
                        <a className="color-main" href="https://khach.nhatro3t.com/">
                            nhatro3t
                        </a>
                    </div>
                </div>
            </footer>
        </>
    );
};

export default Footer;
