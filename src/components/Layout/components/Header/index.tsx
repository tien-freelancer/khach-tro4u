import * as React from 'react';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import LibraryBooksIcon from '@mui/icons-material/LibraryBooks';
import LogoutIcon from '@mui/icons-material/Logout';
import NotificationsIcon from '@mui/icons-material/Notifications';
import PersonIcon from '@mui/icons-material/Person';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import ReceiptIcon from '@mui/icons-material/Receipt';
import SearchIcon from '@mui/icons-material/Search';
import WarningIcon from '@mui/icons-material/Warning';
import HomeIcon from '@mui/icons-material/Home';
import Divider from '@mui/material/Divider';

import { Box, Button, Dialog, DialogContent, DialogTitle, Stack, TextField } from '@mui/material';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import styles from './Header.module.scss';
import images from '../../../../assets/images';
import { removeItem } from '../../../../utils/sessionStorage';
import { storageKeys } from '../../../../constants';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

function Header() {
    const [open, setOpen] = useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const logOut = async () => {
        await removeItem(storageKeys.USER_PHONE_NUMBER);
        window.location.reload();
    };

    return (
        <header className={styles.header}>
            <div className="container">
                <div className={styles.boxHeader}>
                    <Link to="/" className={styles.logo}>
                        <img src={images.logo} alt="" />
                    </Link>

                    <div className={styles.menuHeader}>
                        <div className={`${styles.itemMenu} ${styles.active} ${styles.hasSub}`}>
                            <Link to="/">
                                <HomeIcon />
                                <span>Trang chủ </span>
                            </Link>
                        </div>
                        <div className={styles.itemMenu}>
                            <Link to="/hoadon" className={`${styles.hasSub}`}>
                                <ReceiptIcon />
                                <span>
                                    Hóa đơn <ExpandMoreIcon />
                                </span>
                                <Box className={styles.subMenu}>
                                    <Link to="/">Hóa đơn 1</Link>
                                    <Link to="/">Hóa đơn 2</Link>
                                    <Link to="/">Hóa đơn 3</Link>
                                </Box>
                            </Link>
                        </div>
                        <div className={styles.itemMenu}>
                            <Link to="/hopdong" className={styles.hasSub}>
                                <LibraryBooksIcon />
                                <span>
                                    Hợp đồng <ExpandMoreIcon />
                                </span>
                                <Box className={styles.subMenu}>
                                    <Link to="/">Hop dong 1</Link>
                                    <Link to="/">Hop dong 2</Link>
                                    <Link to="/">Hop dong 3</Link>
                                </Box>
                            </Link>
                        </div>
                        <div className={styles.itemMenu}>
                            <Link to="/suco">
                                <WarningIcon />
                                <span>Sự cố</span>
                            </Link>
                        </div>
                    </div>

                    <div className={styles.actionRight}>
                        <div className={`${styles.hasSub} ${styles.item}`} onClick={handleClickOpen}>
                            <SearchIcon />

                            <Dialog
                                open={open}
                                onClose={handleClose}
                                TransitionComponent={Transition}
                                keepMounted
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                                sx={{
                                    '& .MuiDialog-container': {
                                        alignItems: 'flex-start',
                                        // '& .MuiPaper-root': {
                                        //     width: '100%',
                                        //     maxWidth: '500px',
                                        // },
                                    },
                                }}
                                className={styles.modalSearch}
                            >
                                <DialogTitle>Tìm kiếm</DialogTitle>
                                <DialogContent>
                                    <Box className={styles.boxSearch}>
                                        <TextField
                                            label="Nhập từ khóa cần tìm"
                                            id="outlined-size-small"
                                            defaultValue="Small"
                                            size="small"
                                            className={styles.inputSearch}
                                        />

                                        <Button variant="contained" className={styles.btnSearch}>
                                            Tìm kiếm
                                        </Button>
                                    </Box>
                                </DialogContent>
                            </Dialog>
                        </div>
                        <div className={`${styles.hasSub} ${styles.item}`}>
                            <NotificationsIcon />

                            <Box className={styles.subMenu}>
                                <ul>
                                    <li>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Accusamus impedit</li>
                                    <li>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Accusamus impedit</li>
                                    <li>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Accusamus impedit</li>
                                </ul>
                            </Box>
                        </div>

                        <div className={`${styles.hasSub} ${styles.item}`}>
                            <AccountCircleIcon />
                            <Box className={styles.subMenu}>
                                <div className={styles.avatar}>
                                    <img src="https://dummyimage.com/50x50/bcbcbc" alt="" />
                                </div>
                                <ul>
                                    <li>
                                        <PersonIcon /> Vu Tien
                                    </li>
                                    <li>
                                        <PhoneAndroidIcon /> 0398822724
                                    </li>
                                    <Divider />
                                    <li>
                                        <span className={styles.btnLogout} onClick={() => logOut()}>
                                            <LogoutIcon />
                                            Đăng xuất
                                        </span>
                                    </li>
                                </ul>
                            </Box>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
}

export default Header;
