import { Route, Routes } from 'react-router-dom';
import Footer from './components/Footer';
import Header from './components/Header';

import styles from './Layout.module.scss';

const Layout = ({ children }: any) => {
    return (
        <>
            <Header />
            <main className={styles.main}>{children}</main>

            <Footer />
        </>
    );
};

export default Layout;
