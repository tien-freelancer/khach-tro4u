import { Route, Routes } from 'react-router-dom';
import Footer from '../components/Footer';
import Header from '../components/Header';

import styles from '../Layout.module.scss';
import HeaderDiposit from '../components/HeaderDiposit';

const DipositLayout = ({ children }: any) => {
    return (
        <>
            <HeaderDiposit />

            <main className={styles.main}>{children}</main>

            <Footer />
        </>
    );
};

export default DipositLayout;
