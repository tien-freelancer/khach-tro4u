import { Link } from 'react-router-dom'
import styles from './Button.module.scss'

function Button({
    to,
    href,
    primary,
    outline,
    disabled = false,
    rounded = false,
    small,
    large,
    text = false,
    children,
    leftIcon,
    rightIcon,
    onClick,
    className,
    ...passProps
}) {
    let Comp = 'button';
    const props = {
        onClick,
        ...passProps,
    }

    // Remove event listener when button is disabled
    if (disabled) {
        // delete props.onClick
        Object.keys(props).forEach((key) => {
            if (key.startsWith && typeof props[key] === 'function') {
                delete props[key]
            }
        })
    }

    if (to) {
        props.to = to
        Comp = Link
    } else if (href) {
        props.href = href
        Comp = 'a'
    }

    const classes = 'wrapper'

    return (
        <Comp
            className={
                `${styles[classes]} ${primary ? styles.primary : ''} ${outline ? styles.outline : ''} ${disabled ? styles.disabled : ''} ${small ? styles.small : ''} ${large ? styles.large : ''}
             ${rounded ? styles.rounded : ''}
             ${text ? styles.text : ''}
             ${className ? className : ''}
            `}
            {...props}
        >
            {leftIcon && <span className={styles.icon}>{leftIcon}</span>}
            <span> {children}</span>
            {rightIcon && <span className={styles.icon}>{rightIcon}</span>}
        </Comp>
    );
}

export default Button;