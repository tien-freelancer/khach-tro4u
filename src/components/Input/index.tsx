import { AccountCircle } from '@mui/icons-material';
import { Input, InputAdornment, TextField } from '@mui/material';

interface Props {
    InputProps: any;
    iconEnd: JSX.Element;
}

const IconTextField = (props: Props) => {
    const { InputProps, iconEnd } = props;

    return (
        <Input
            id="input-with-icon-adornment"
            startAdornment={
                <InputAdornment position="start">
                    <AccountCircle />
                </InputAdornment>
            }
        />
    );
};

export default IconTextField;
