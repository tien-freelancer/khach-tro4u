import { post } from './axiosClient';

export const contractDetail = (body: any) => {
    return post('/hopdong/get-chi-tiet-hop-dong', body);
};
