import axios from 'axios';
import { getObjectData, getStringData } from '../utils/localStorage';
import { storageKeys } from '../redux/config';

let jsonDataUser: any = getObjectData(storageKeys.USER_TOKEN);
const token = jsonDataUser ? jsonDataUser?.accessToken : null;
const apiKey = 'Bearer';
const baseURL = `${process.env.REACT_APP_END_POINT_URL}/api/version/1.0`;

console.log(baseURL);

let headers: {} = {
    'Content-Type': 'application/json',
    accept: 'application/json',
    'Access-Control-Allow-Origin': '*',
};

export const get = (endPoint: any, params?: any, isAuth?: boolean) => {
    if (isAuth) {
        return axios.get(baseURL + endPoint, {
            headers: {
                ...headers,
                Authorization: `${apiKey} ${token}`,
            },
            params,
            // withCredentials: true,
        });
    } else {
        return axios.get(baseURL + endPoint, {
            headers,
            params,
            // withCredentials: true,
        });
    }
};

export const post = (endPoint: string, body: any, params?: any, isAuth?: boolean, isRegister?: boolean) => {
    if (isAuth) {
        return axios.post(baseURL + endPoint, body, {
            headers: {
                ...headers,
                Authorization: `${apiKey} ${token}`,
            },
            params,
            // withCredentials: true,
        });
    } else if (isRegister) {
        return axios.post(baseURL + endPoint, body, {
            headers: {
                ...headers,
            },
            params,
            // withCredentials: true,
        });
    } else {
        return axios.post(baseURL + endPoint, body, {
            headers,
            params,
            // withCredentials: true,
        });
    }
};

export const put = (endPoint: string, body: any, params?: any, isAuth?: boolean) => {
    if (isAuth) {
        return axios.put(baseURL + endPoint, body, {
            headers: {
                ...headers,
                Authorization: `${apiKey} ${token}`,
            },
            params,
            // withCredentials: true,
        });
    } else {
        return axios.put(baseURL + endPoint, body, {
            headers,
            params,
            // withCredentials: true,
        });
    }
};

export const clear = (endPoint: string, params?: any, isAuth?: boolean) => {
    if (isAuth) {
        return axios.delete(baseURL + endPoint, {
            headers: {
                ...headers,
                Authorization: token,
            },
            params,
            // withCredentials: true,
        });
    } else {
        return axios.delete(baseURL + endPoint, {
            headers,
            params,
            // withCredentials: true,
        });
    }
};

export const getTemp = (endPoint: string, params?: any, isAuth?: boolean) => {
    if (isAuth) {
        return axios.get(baseURL + endPoint, {
            headers: {
                ...headers,
                Authorization: `${apiKey} ${token}`,
            },
            params,
        });
    } else {
        return axios.get(baseURL + endPoint, {
            headers,
            params,
        });
    }
};
