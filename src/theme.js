import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
    palette: {
        primary: {
            main: '#15a35e',
            // light: will be calculated from palette.primary.main,
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
            main: '#E0C2FF',
            light: '#F5EBFF',
            // dark: will be calculated from palette.secondary.main,
            contrastText: '#47008F',
        },
        gray: {
            main: '#777'
        }
    },
    components: {
        MuiInputLabel: {
            styleOverrides: {
                // Name of the slot
                root: {
                    // Some CSS
                    color: '#777',
                },
            },
        },
        MuiTextField: {
            styleOverrides: {
                root: {
                    '& svg': {
                        color: '#777',
                    },

                },
            },
        },

        MuiDialog: {
            styleOverrides: {
                root: {
                    '& .MuiDialog-container': {
                        alignItems: 'center',
                    },
                    '& .MuiDialog-paper': {
                        maxWidth: '360px',
                        // maxWidth: '500px',
                    }
                },

            },
        },

    },
});