import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from "react-redux";
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import GlobalStyles from './components/GlobalStyles';
import { ThemeProvider } from '@mui/material';
import { theme } from './theme';
import { BrowserRouter } from 'react-router-dom';
import {configureStore} from "./redux/store";
import { createBrowserHistory } from 'history';

// tslint:disable-next-line:no-namespace
declare global {
    interface Window {
      // tslint:disable-next-line:no-any
      devToolsExtension: any;
    }
  }
const history = createBrowserHistory();
const store = configureStore(history);

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
    <React.StrictMode>
        <BrowserRouter>
            <GlobalStyles>
                <ThemeProvider theme={theme}>
                    <Provider store={ store }>
                        <App />
                    </Provider>
                </ThemeProvider>
            </GlobalStyles>
        </BrowserRouter>
    </React.StrictMode>,
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
