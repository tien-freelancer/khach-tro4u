export const ssrMode = typeof window === 'undefined';

export const ApiUrl = process.env.NEXT_PUBLIC_VME_API_GATEWAY_URL;

const baseUrl = '/';

export const storageKeys = {
    USER_DATA: 'user-data',
    USER_TOKEN: 'user-token',
    USER_PHONE_NUMBER: '3t-user-phone-number',
};
